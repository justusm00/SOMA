/* Copyright (C) 2016-2021 Ludwig Schneider

 This file is part of SOMA.

 SOMA is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 SOMA is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with SOMA.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "polytype_conversion.h"

#include <stdlib.h>
#include <hdf5.h>
#include "phase.h"
#include "io.h"
#include "mesh.h"
#include <time.h>

//! \file polytype_conversion.c
//! \brief Implementation of polytype_conversion.h

int read_poly_conversion_hdf5(struct Phase *const p, const hid_t file_id, const hid_t plist_id)
{

    p->pc.deltaMC = 0;
    p->pc.array = NULL;
    p->pc.len_reactions = 0;
    p->pc.len_dependencies = 0;
    p->pc.num_conversions = NULL;
    p->pc.rate = NULL;
    p->pc.input_type = NULL;
    p->pc.output_type = NULL;
    p->pc.reaction_end = NULL;
    p->pc.dependency_ntype = NULL;
    p->pc.dependency_type = NULL;
    p->pc.dependency_type_offset = NULL;
    //Quick exit if no poly conversion is present in the file
    if (!(H5Lexists(file_id, "/polyconversion", H5P_DEFAULT) > 0))
        return 0;

    herr_t status;
    unsigned int tmp_deltaMC;
    status = read_hdf5(file_id, "polyconversion/deltaMC", H5T_STD_U32LE, plist_id, &tmp_deltaMC);
    HDF5_ERROR_CHECK(status);

    //Quick exit if no conversion updates are scheduled
    if (tmp_deltaMC == 0)
        return 0;

#if ( ENABLE_MONOTYPE_CONVERSIONS == 1 )
    fprintf(stderr,
            "ERROR: %s: %d, Monotype Conversions are activated so polytype conversions do not work in the current implementation. Switch off the this feature and rerun.",
            __FILE__, __LINE__);
    return -1;
#endif                          //ENABLE_MONOTYPE_CONVERSIONS

    //Read the conversion list before reading the spatial array.
    const hid_t dset_input = H5Dopen(file_id, "/polyconversion/input_type", H5P_DEFAULT);
    HDF5_ERROR_CHECK(dset_input);
    const hid_t dspace_input = H5Dget_space(dset_input);
    HDF5_ERROR_CHECK(dspace_input);
    const unsigned int ndims_input = H5Sget_simple_extent_ndims(dspace_input);
    if (ndims_input != 1)
        {
            fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s.\n",
                    __FILE__, __LINE__, "polyconversion/input_type");
            return -1;
        }
    hsize_t dim_input;
    status = H5Sget_simple_extent_dims(dspace_input, &dim_input, NULL);
    HDF5_ERROR_CHECK(status);

    const hid_t dset_output = H5Dopen(file_id, "/polyconversion/output_type", H5P_DEFAULT);
    HDF5_ERROR_CHECK(dset_output);
    const hid_t dspace_output = H5Dget_space(dset_output);
    HDF5_ERROR_CHECK(dspace_output);
    const unsigned int ndims_output = H5Sget_simple_extent_ndims(dspace_output);
    if (ndims_output != 1)
        {
            fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s.\n",
                    __FILE__, __LINE__, "polyconversion/output_type");
            return -1;
        }
    hsize_t dim_output;
    status = H5Sget_simple_extent_dims(dspace_output, &dim_output, NULL);
    HDF5_ERROR_CHECK(status);

    const hid_t dset_end = H5Dopen(file_id, "/polyconversion/end", H5P_DEFAULT);
    HDF5_ERROR_CHECK(dset_end);
    const hid_t dspace_end = H5Dget_space(dset_end);
    HDF5_ERROR_CHECK(dspace_end);
    const unsigned int ndims_end = H5Sget_simple_extent_ndims(dspace_end);
    if (ndims_end != 1)
        {
            fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s.\n",
                    __FILE__, __LINE__, "polyconversion/end_type");
            return -1;
        }
    hsize_t dim_end;
    status = H5Sget_simple_extent_dims(dspace_end, &dim_end, NULL);
    HDF5_ERROR_CHECK(status);

    if (dim_input != dim_output)
        {
            fprintf(stderr,
                    "ERROR: %s:%d the length of the input and output type for poly conversions is not equal %d %d\n",
                    __FILE__, __LINE__, (int)dim_input, (int)dim_output);
            return -3;
        }
    if (dim_input != dim_end)
        {
            fprintf(stderr,
                    "ERROR: %s:%d the length of the input type and end for poly conversions is not equal %d %d\n",
                    __FILE__, __LINE__, (int)dim_input, (int)dim_end);
            return -3;
        }

    p->pc.input_type = (unsigned int *)malloc(dim_input * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.input_type, dim_input * sizeof(unsigned int));
    p->pc.output_type = (unsigned int *)malloc(dim_output * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.output_type, dim_output * sizeof(unsigned int));
    p->pc.reaction_end = (unsigned int *)malloc(dim_end * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.reaction_end, dim_end * sizeof(unsigned int));
    p->pc.num_conversions = (unsigned int *)malloc(p->n_poly_type * p->n_poly_type * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.num_conversions,p->n_poly_type * p->n_poly_type * sizeof(unsigned int));

    for(int i = 0; i < p->n_poly_type * p->n_poly_type;i++)
        {
            p->pc.num_conversions[i]=0;
        }


    p->pc.len_reactions = dim_input;

    //Actuablly read the polyconversion list data
    status = H5Dread(dset_input, H5T_STD_U32LE, H5S_ALL, H5S_ALL, plist_id, p->pc.input_type);
    HDF5_ERROR_CHECK(status);
    status = H5Sclose(dspace_input);
    HDF5_ERROR_CHECK(status);
    status = H5Dclose(dset_input);
    HDF5_ERROR_CHECK(status);
    status = H5Dread(dset_output, H5T_STD_U32LE, H5S_ALL, H5S_ALL, plist_id, p->pc.output_type);
    HDF5_ERROR_CHECK(status);
    status = H5Sclose(dspace_output);
    HDF5_ERROR_CHECK(status);
    status = H5Dclose(dset_output);
    HDF5_ERROR_CHECK(status);
    status = H5Dread(dset_end, H5T_STD_U32LE, H5S_ALL, H5S_ALL, plist_id, p->pc.reaction_end);
    HDF5_ERROR_CHECK(status);
    status = H5Sclose(dspace_end);
    HDF5_ERROR_CHECK(status);
    status = H5Dclose(dset_end);
    HDF5_ERROR_CHECK(status);

    //Read the array information
    const unsigned int my_domain = p->info_MPI.sim_rank / p->info_MPI.domain_size;
    const unsigned int ghost_buffer_size = p->args.domain_buffer_arg * p->ny * p->nz;

    const hsize_t hsize_memspace[3] = { p->nx / p->args.N_domains_arg, p->ny, p->nz };
    hid_t memspace = H5Screate_simple(3, hsize_memspace, NULL);
    hid_t dataset = H5Dopen2(file_id, "/polyconversion/array", H5P_DEFAULT);
    p->pc.array =
        (uint8_t *) malloc((p->nx / p->args.N_domains_arg + 2 * p->args.domain_buffer_arg) * p->ny * p->nz *
                           sizeof(uint8_t));
    if (p->pc.array == NULL)
        {
            fprintf(stderr, "ERROR: Malloc %s:%d\n", __FILE__, __LINE__);
            return -1;
        }

    hid_t dataspace = H5Dget_space(dataset);
    const hsize_t offset[3] = { my_domain * (p->nx / p->args.N_domains_arg), 0, 0 };
    H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, offset, NULL, hsize_memspace, NULL);

    if ((status = H5Dread(dataset, H5T_STD_U8LE, memspace, dataspace, plist_id, p->pc.array + ghost_buffer_size)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

#if ( ENABLE_MPI == 1 )
    const int left_neigh_rank =
        (((my_domain - 1) + p->args.N_domains_arg) % p->args.N_domains_arg) * p->info_MPI.domain_size +
        p->info_MPI.domain_rank;
    const int right_neigh_rank =
        (((my_domain + 1) + p->args.N_domains_arg) % p->args.N_domains_arg) * p->info_MPI.domain_size +
        p->info_MPI.domain_rank;

    MPI_Request req[4];
    MPI_Status stat[4];

    uint8_t *ptr = p->pc.array + ghost_buffer_size;
    MPI_Isend(ptr, ghost_buffer_size, MPI_UINT8_T, left_neigh_rank, 0, p->info_MPI.SOMA_comm_sim, req + 0);
    ptr = p->pc.array + ((p->nx / p->args.N_domains_arg) * p->ny * p->nz);
    MPI_Isend(ptr, ghost_buffer_size, MPI_UINT8_T, right_neigh_rank, 1, p->info_MPI.SOMA_comm_sim, req + 1);

    ptr = p->pc.array;
    MPI_Irecv(ptr, ghost_buffer_size, MPI_UINT8_T, left_neigh_rank, 1, p->info_MPI.SOMA_comm_sim, req + 2);
    ptr = p->pc.array + ((p->nx / p->args.N_domains_arg) * p->ny * p->nz) + ghost_buffer_size;
    MPI_Irecv(ptr, ghost_buffer_size, MPI_UINT8_T, right_neigh_rank, 0, p->info_MPI.SOMA_comm_sim, req + 3);

    MPI_Waitall(4, req, stat);
    MPI_Barrier(p->info_MPI.SOMA_comm_sim);
#endif                          //ENABLE_MPI

    if ((status = H5Sclose(dataspace)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    if ((status = H5Sclose(memspace)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    if ((status = H5Dclose(dataset)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    //If rate is defined in the hdf5, partial conversions are activated and "rate", "n_density_dependencies", "density_dependencies" are read.
    if (!(H5Lexists(file_id, "/polyconversion/rate", H5P_DEFAULT) > 0))
        {
            //Enable the updat only if everything worked fine so far
            p->pc.deltaMC = tmp_deltaMC;
            return 0;
        }

    const hid_t dset_rate = H5Dopen(file_id, "/polyconversion/rate", H5P_DEFAULT);
    HDF5_ERROR_CHECK(dset_rate);
    const hid_t dspace_rate = H5Dget_space(dset_rate);
    HDF5_ERROR_CHECK(dspace_rate);
    const unsigned int ndims_rate = H5Sget_simple_extent_ndims(dspace_rate);
    if (ndims_rate != 1)
        {
            fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s.\n",
                    __FILE__, __LINE__, "polyconversion/rate");
            return -1;
        }
    hsize_t dim_rate;
    status = H5Sget_simple_extent_dims(dspace_rate, &dim_rate, NULL);
    HDF5_ERROR_CHECK(status);

    const hid_t dset_ndependency = H5Dopen(file_id, "/polyconversion/n_density_dependencies", H5P_DEFAULT);
    HDF5_ERROR_CHECK(dset_ndependency);
    const hid_t dspace_ndependency = H5Dget_space(dset_ndependency);
    HDF5_ERROR_CHECK(dspace_ndependency);
    const unsigned int ndims_ndependency = H5Sget_simple_extent_ndims(dspace_ndependency);
    if (ndims_ndependency != 1)
        {
            fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s.\n",
                    __FILE__, __LINE__, "polyconversion/n_density_dependencies");
            return -1;
        }
    hsize_t dim_ndependency;
    status = H5Sget_simple_extent_dims(dspace_ndependency, &dim_ndependency, NULL);
    HDF5_ERROR_CHECK(status);

    const hid_t dset_dependency = H5Dopen(file_id, "/polyconversion/density_dependencies", H5P_DEFAULT);
    HDF5_ERROR_CHECK(dset_dependency);
    const hid_t dspace_dependency = H5Dget_space(dset_dependency);
    HDF5_ERROR_CHECK(dspace_dependency);
    const unsigned int ndims_dependency = H5Sget_simple_extent_ndims(dspace_dependency);
    if (ndims_dependency != 1)
        {
            fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s.\n",
                    __FILE__, __LINE__, "polyconversion/density_dependencies");
            return -1;
        }
    hsize_t dim_dependency;
    status = H5Sget_simple_extent_dims(dspace_dependency, &dim_dependency, NULL);
    HDF5_ERROR_CHECK(status);

    if (dim_input != dim_rate)
        {
            fprintf(stderr,
                    "ERROR: %s:%d the length of the input type and rate for poly conversions is not equal %d %d\n",
                    __FILE__, __LINE__, (int)dim_input, (int)dim_rate);
            return -3;
        }
    if (dim_input != dim_ndependency)
        {
            fprintf(stderr,
                    "ERROR: %s:%d the length of the input type and rate for poly conversions is not equal %d %d\n",
                    __FILE__, __LINE__, (int)dim_input, (int)dim_rate);
            return -3;
        }

    p->pc.rate = (soma_scalar_t *) malloc(dim_rate * sizeof(soma_scalar_t));
    MALLOC_ERROR_CHECK(p->pc.rate, dim_rate * sizeof(soma_scalar_t));
    p->pc.dependency_ntype = (unsigned int *)malloc(dim_ndependency * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.dependency_ntype, dim_ndependency * sizeof(unsigned int));
    p->pc.dependency_type_offset = (unsigned int *)malloc(dim_ndependency * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.dependency_type_offset, dim_ndependency * sizeof(unsigned int));
    p->pc.dependency_type = (unsigned int *)malloc(dim_dependency * sizeof(unsigned int));
    MALLOC_ERROR_CHECK(p->pc.dependency_type, dim_dependency * sizeof(unsigned int));


    p->pc.len_dependencies = dim_dependency;

    //Read rate and dependencies:
    status = H5Dread(dset_rate, H5T_SOMA_NATIVE_SCALAR, H5S_ALL, H5S_ALL, plist_id, p->pc.rate);
    HDF5_ERROR_CHECK(status);
    status = H5Sclose(dspace_rate);
    HDF5_ERROR_CHECK(status);
    status = H5Dclose(dset_rate);
    HDF5_ERROR_CHECK(status);
    status = H5Dread(dset_ndependency, H5T_STD_U32LE, H5S_ALL, H5S_ALL, plist_id, p->pc.dependency_ntype);
    HDF5_ERROR_CHECK(status);
    status = H5Sclose(dspace_ndependency);
    HDF5_ERROR_CHECK(status);
    status = H5Dclose(dset_ndependency);
    HDF5_ERROR_CHECK(status);
    p->pc.dependency_type_offset[0] = 0;
    for (unsigned int i = 1; i < dim_ndependency; i++)
        {
            p->pc.dependency_type_offset[i] = p->pc.dependency_type_offset[i - 1] + p->pc.dependency_ntype[i - 1];
        }
    if (p->pc.len_dependencies > 0)
        {
            status = H5Dread(dset_dependency, H5T_STD_U32LE, H5S_ALL, H5S_ALL, plist_id, p->pc.dependency_type);
            HDF5_ERROR_CHECK(status);
        }
    status = H5Sclose(dspace_dependency);
    HDF5_ERROR_CHECK(status);
    status = H5Dclose(dset_dependency);
    HDF5_ERROR_CHECK(status);

    //Enable the updat only if everything worked fine so far
    p->pc.deltaMC = tmp_deltaMC;

    return 0;
}

int write_poly_conversion_hdf5(const struct Phase *const p, const hid_t file_id, const hid_t plist_id)
{
    //Quick exit for no poly conversions
    if (p->pc.deltaMC == 0)
        return 0;
    hid_t group = H5Gcreate2(file_id, "/polyconversion", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    const hsize_t one = 1;
    status =
        write_hdf5(1, &one, file_id, "/polyconversion/deltaMC", H5T_STD_U32LE, H5T_NATIVE_UINT, plist_id,
                   &(p->pc.deltaMC));
    HDF5_ERROR_CHECK(status);

    const hsize_t list_len = p->pc.len_reactions;
    status =
        write_hdf5(1, &list_len, file_id, "/polyconversion/input_type", H5T_STD_U32LE, H5T_NATIVE_UINT, plist_id,
                   p->pc.input_type);
    HDF5_ERROR_CHECK(status);
    status =
        write_hdf5(1, &list_len, file_id, "/polyconversion/output_type", H5T_STD_U32LE, H5T_NATIVE_UINT, plist_id,
                   p->pc.output_type);
    HDF5_ERROR_CHECK(status);
    status =
        write_hdf5(1, &list_len, file_id, "/polyconversion/end", H5T_STD_U32LE, H5T_NATIVE_UINT, plist_id,
                   p->pc.reaction_end);
    HDF5_ERROR_CHECK(status);

    if (p->pc.rate != NULL)
        {
            status =
                write_hdf5(1, &list_len, file_id, "/polyconversion/rate", H5T_SOMA_NATIVE_SCALAR,
                           H5T_SOMA_NATIVE_SCALAR, plist_id, p->pc.rate);
            HDF5_ERROR_CHECK(status);
            status =
                write_hdf5(1, &list_len, file_id, "/polyconversion/n_density_dependencies", H5T_STD_U32LE,
                           H5T_NATIVE_UINT, plist_id, p->pc.dependency_ntype);
            HDF5_ERROR_CHECK(status);
            const hsize_t dep_list_len = p->pc.len_dependencies;
            if (dep_list_len > 0)
                {
                    status =
                        write_hdf5(1, &dep_list_len, file_id, "/polyconversion/density_dependencies", H5T_STD_U32LE,
                                   H5T_NATIVE_UINT, plist_id, p->pc.dependency_type);
                    HDF5_ERROR_CHECK(status);
                }
            else
                {               //no dependencies --> empty dataset, so only create, don't write.
                    herr_t status;
                    const hid_t dataspace = H5Screate_simple(1, &dep_list_len, NULL);
                    HDF5_ERROR_CHECK(dataspace);
                    const hid_t dataset =
                        H5Dcreate2(file_id, "/polyconversion/density_dependencies", H5T_STD_U32LE, dataspace,
                                   H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                    HDF5_ERROR_CHECK(dataset);
                    status = H5Sclose(dataspace);
                    HDF5_ERROR_CHECK(status);
                    status = H5Dclose(dataset);
                    HDF5_ERROR_CHECK(status);
                }
        }

    //Write the array to disk
    const unsigned int my_domain = p->info_MPI.sim_rank / p->info_MPI.domain_size;
    const unsigned int ghost_buffer_size = p->args.domain_buffer_arg * p->ny * p->nz;

    const hsize_t hsize_dataspace[3] = { p->nx, p->ny, p->nz };
    hid_t dataspace = H5Screate_simple(3, hsize_dataspace, NULL);
    const hsize_t hsize_memspace[3] = { p->nx / p->args.N_domains_arg, p->ny, p->nz };
    hid_t memspace = H5Screate_simple(3, hsize_memspace, NULL);

    hid_t dataset =
        H5Dcreate2(file_id, "/polyconversion/array", H5T_STD_U8LE, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    dataspace = H5Dget_space(dataset);
    const hsize_t offset[3] = { my_domain * (p->nx / p->args.N_domains_arg), 0, 0 };
    H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, offset, NULL, hsize_memspace, NULL);

#if ( ENABLE_MPI == 1 )
    MPI_Barrier(p->info_MPI.SOMA_comm_world);
#endif                          //ENABLE_MPI

    if ((status =
         H5Dwrite(dataset, H5T_NATIVE_UINT8, memspace, dataspace, plist_id, p->pc.array + ghost_buffer_size)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    if ((status = H5Sclose(dataspace)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    if ((status = H5Sclose(memspace)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    if ((status = H5Dclose(dataset)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    if ((status = H5Gclose(group)) < 0)
        {
            fprintf(stderr, "ERROR: core: %d HDF5-error %s:%d code %d\n",
                    p->info_MPI.world_rank, __FILE__, __LINE__, status);
            return status;
        }

    return 0;
}

int copyin_poly_conversion(struct Phase *p)
{
    if (p->pc.deltaMC != 0)
        {
#ifdef _OPENACC
            //The pc struct itself is part of the phase struct and is already present of the device
#pragma acc enter data copyin(p->pc.array[0:p->n_cells_local])
#pragma acc enter data copyin(p->pc.input_type[0:p->pc.len_reactions])
#pragma acc enter data copyin(p->pc.output_type[0:p->pc.len_reactions])
#pragma acc enter data copyin(p->pc.reaction_end[0:p->pc.len_reactions])
#pragma acc enter data copyin(p->pc.num_conversions[0:p->n_poly_type*p->n_poly_type])
#pragma acc enter data copyin(p->umbrella_field[0:p->n_cells_local*p->n_types])
            if (p->pc.rate != NULL)
                {
#pragma acc enter data copyin(p->pc.rate[0:p->pc.len_reactions])
#pragma acc enter data copyin(p->pc.dependency_ntype[0:p->pc.len_reactions])
#pragma acc enter data copyin(p->pc.dependency_type_offset[0:p->pc.len_reactions])
#pragma acc enter data copyin(p->pc.dependency_type[0:p->pc.len_dependencies])
                }
#endif                          //_OPENACC
        }
    return 0;
}

int copyout_poly_conversion(struct Phase *p)
{
    if (p->pc.deltaMC != 0)
        {
#ifdef _OPENACC
#pragma acc exit data copyout(p->pc.array[0:p->n_cells_local])
#pragma acc exit data copyout(p->pc.input_type[0:p->pc.len_reactions])
#pragma acc exit data copyout(p->pc.output_type[0:p->pc.len_reactions])
#pragma acc exit data copyout(p->pc.reaction_end[0:p->pc.len_reactions])
#pragma acc exit data copyout(p->pc.num_conversions[0:p->n_poly_type*p->n_poly_type])
#pragma acc exit data copyout(p->umbrella_field[0:p->n_cells_local*p->n_types])
            if (p->pc.rate != NULL)
                {
#pragma acc exit data copyout(p->pc.rate[0:p->pc.len_reactions])
#pragma acc exit data copyout(p->pc.dependency_ntype[0:p->pc.len_reactions])
#pragma acc exit data copyout(p->pc.dependency_type_offset[0:p->pc.len_reactions])
#pragma acc exit data copyout(p->pc.dependency_type[0:p->pc.len_dependencies])
                }
#endif                          //_OPENACC
        }
    return 0;
}

int update_self_poly_conversion(const struct Phase *const p)
{
    if (p->pc.deltaMC != 0)
        {
#ifdef _OPENACC
#pragma acc update self(p->pc.array[0:p->n_cells_local])
#pragma acc update self(p->pc.input_type[0:p->pc.len_reactions])
#pragma acc update self(p->pc.output_type[0:p->pc.len_reactions])
#pragma acc update self(p->pc.reaction_end[0:p->pc.len_reactions])
#pragma acc update self(p->pc.num_conversions[0:p->n_poly_type*p->n_poly_type])
#pragma acc update self(p->umbrella_field[0:p->n_cells_local*p->n_types])
            if (p->pc.rate != NULL)
                {
#pragma acc update self(p->pc.rate[0:p->pc.len_reactions])
#pragma acc update self(p->pc.dependency_ntype[0:p->pc.len_reactions])
#pragma acc update self(p->pc.dependency_type_offset[0:p->pc.len_reactions])
#pragma acc update self(p->pc.dependency_type[0:p->pc.len_dependencies])
                }
#endif                          //_OPENACC
        }
    return 0;
}

int free_poly_conversion(struct Phase *p)
{
    free(p->pc.array);
    free(p->pc.input_type);
    free(p->pc.output_type);
    free(p->pc.rate);
    free(p->pc.dependency_ntype);
    free(p->pc.dependency_type_offset);
    free(p->pc.dependency_type);
    free(p->pc.num_conversions);

    return 0;
}


int convert_polytypes(struct Phase *p)
{
    //Quick exit for
    static unsigned int last_time = 0;
    if (last_time >= p->time)
        return 0;
    last_time = p->time;
    update_polymer_rcm(p);
    if (p->umbrella_field != NULL)
        {
            //do polymer conversion only with root rank
            if (p->info_MPI.sim_rank == 0) 
                {
                    return optimize_boundaries(p);

                }
            
        }
    else
        {
            printf("ERROR: No umbrella field available \n");
            return -1;
        }

/*     if (p->pc.rate == NULL)
        {
            return fully_convert_polytypes(p);
        }
    else
        {
            return partially_convert_polytypes(p);
        } */
}

int fully_convert_polytypes(struct Phase *p)
{
//Iterate all polymers and apply the reaction rules
#pragma acc parallel loop present(p[0:1])
#pragma omp parallel for
    for (uint64_t poly = 0; poly < p->n_polymers; poly++)
        {
            const Monomer rcm = p->polymers[poly].rcm;
            const uint64_t cell = coord_to_index(p, rcm.x, rcm.y, rcm.z);


            if (p->pc.array[cell] != 0)
                {
                    //Minus 1 because the index in array are shifted by 1
                    int i = p->pc.array[cell] - 1;
                    if (p->polymers[poly].type == p->pc.input_type[i])
                        {
#pragma acc atomic update
                            p->pc.num_conversions[p->polymers[poly].type * p->n_poly_type + p->pc.output_type[i]]++;                            
                            p->polymers[poly].type = p->pc.output_type[i];
                        }
                    for (i++; !p->pc.reaction_end[i - 1]; i++)
                        {
                            if (p->polymers[poly].type == p->pc.input_type[i])
                            {
#pragma acc atomic update
                                p->pc.num_conversions[p->polymers[poly].type * p->n_poly_type + p->pc.output_type[i]]++;                                     
                                p->polymers[poly].type = p->pc.output_type[i];

                            }
                        }
                }
        }
    return 0;
}

int partially_convert_polytypes(struct Phase *p)
{
    //Iterate all polymers and apply the reaction rules
#pragma acc parallel loop present(p[0:1])
#pragma omp parallel for
    for (uint64_t poly = 0; poly < p->n_polymers; poly++)
        {
            Polymer *mypoly = p->polymers + poly;
            const Monomer rcm = mypoly->rcm;
            const uint64_t cell = coord_to_index(p, rcm.x, rcm.y, rcm.z);

            if (p->pc.array[cell] != 0)
                {
                    int i = p->pc.array[cell] - 2;
                    do
                        {
                            i++;
                            if (mypoly->type == p->pc.input_type[i])
                                {
                                    soma_scalar_t probability = p->pc.rate[i] * p->fields_unified[mypoly->type * p->n_cells_local +cell] * p->field_scaling_type[mypoly->type];;
                                    soma_scalar_t random_number = soma_rng_soma_scalar(&(mypoly->poly_state), p);
                                    if (random_number < probability)
                                        {
#pragma acc atomic update
                                            p->pc.num_conversions[p->polymers[poly].type * p->n_poly_type + p->pc.output_type[i]]++;                                             
                                            p->polymers[poly].type = p->pc.output_type[i];      //got modifiable lvalue compile error when using mypoly->type = ... and was not able to fix this otherwise.
                                            break;      //to continue with next polymer if conversion has taken place.
                                        }
                                }
                    } while (!p->pc.reaction_end[i]);
                }
        }

    return 0;
}

int optimize_boundaries(struct Phase *p)
{

#pragma acc update self(p->fields_unified[0:p->n_cells_local*p->n_types])
#pragma acc update self(p->polymers[0:p->n_polymers])
    update_self_polymer_heavy(p, 0); 
    uint64_t num_poly_flippable = 0; // number of flippable polymers
    uint64_t total_flip_attempts = 0; //total number of flip attempts
    uint64_t total_flips_accepted = 0; //total number of accepted flip attempts
    soma_scalar_t total_cost = 0.0; 



    // arrays and constants
    int64_t * poly_isflippable = (int64_t *)malloc( p->n_polymers* sizeof(int64_t)); //Boolean array that stores whether or not polymer has monomers in target density area.
    int64_t * delta_fields_unified_best = (int64_t *)malloc(p->n_types * p->n_cells_local * sizeof(int64_t)); //array that stores changes in density (best values)
    



    //initialize poly_isflippable with zeros
    for (uint64_t poly = 0; poly < p->n_polymers; poly++) poly_isflippable[poly]=0;



    //get flippable polymers
    num_poly_flippable = get_flip_candidates(p, poly_isflippable);

    //allocate several arrays needed for flipping
    int64_t * poly_flippable_indices = (int64_t *)malloc(num_poly_flippable * sizeof(int64_t)); //array that contains indices of flippable polymers
    int64_t * poly_cell_indices = (int64_t *)malloc(num_poly_flippable * p->reference_Nbeads * sizeof(int64_t)); //Array of length num_poly_flippable * p->reference_Nbeads that stores cell indices in which a polymer has monomers.
    int64_t * poly_cell_num = (int64_t *)malloc(num_poly_flippable  * p->n_poly_type* p->n_types * p->reference_Nbeads * sizeof(int64_t)); //Array of length num_poly_flippable * p->reference_Nbeads * p->n_types * p->n_poly_type that stores number of monomers in cells corresponding to poly_cell_indices for each possible polymer type. 
    unsigned int * poly_types_best=(unsigned int *)malloc(num_poly_flippable * sizeof(unsigned int)); //array that stores polymer types (best values)
    

    //save indices of polymers with monomers in conversion zone sequentially to poly_flippable_indices
    uint64_t k = 0; 
    for (uint64_t poly = 0; poly < p->n_polymers; poly++)
        {
            
            if(poly_isflippable[poly]==1)
                {
                    poly_flippable_indices[k]=poly;
                    k++;
                }
        }

    //initialize poly_cell_indices with -1's
    for (uint64_t i = 0; i < num_poly_flippable * p->reference_Nbeads; i++) poly_cell_indices[i] = -1;

    //initialize poly_cell_num with 0's (not really necessary)
    for (uint64_t i = 0; i < num_poly_flippable * p->reference_Nbeads * p->n_poly_type* p->n_types; i++) poly_cell_num[i] = 0;

    //get cell information
    get_cell_info(p,num_poly_flippable,poly_flippable_indices,poly_cell_indices,poly_cell_num);

    // intialize delta_fields_unified

    for (uint64_t cell = 0; cell < p->n_cells_local; cell++)
        {
            for(uint64_t type = 0; type < p->n_types; type++)
                {
                    delta_fields_unified_best[type*p->n_cells_local + cell] = 0;
                }
        }


    // inititalize poly_types
    for (uint64_t poly = 0; poly < num_poly_flippable; poly++)
        {
            poly_types_best[poly]=p->polymers[poly_flippable_indices[poly]].type;
        }
    
    //initialize cost
    total_cost=get_cost_composition(p);

    

    //printf("Start configuration optimization at t=%d on testing branch\n",p->time);
    printf("MSE before optimization %f \n",total_cost);   

    //do some more flips at T=0
    total_cost = flip_polytypes(p,total_cost, num_poly_flippable, &total_flip_attempts, &total_flips_accepted, poly_cell_indices, poly_cell_num,
                                poly_flippable_indices,delta_fields_unified_best, poly_types_best);

    printf("MSE after flips at T=0: %f \n",total_cost);


    if ( p->Tmax > 0)
        {
            //get new cost value from simulated annealing
            total_cost = simulated_annealing(p,total_cost, num_poly_flippable, &total_flip_attempts, &total_flips_accepted,  poly_cell_indices,poly_cell_num,
                                        poly_flippable_indices,   delta_fields_unified_best, poly_types_best);
            printf("MSE after annealing: %f \n",total_cost); 
        }





    printf("Flip attempts: %d\n",total_flip_attempts);
    printf("Flippable polymers: %d\n",num_poly_flippable);

    //printf("Polymers flipped: %llu\n",total_flip_attempts);
    //printf("Accepted flips: %llu\n",total_flips_accepted);
    //printf("Flippable polymers: %llu\n",num_poly_flippable);

            //update polymer types
#pragma acc enter data copyin(poly_flippable_indices[0:num_poly_flippable])
#pragma acc enter data copyin(poly_types_best[0:num_poly_flippable])
#pragma acc parallel loop present(p[0:1], poly_flippable_indices[0:num_poly_flippable], poly_types_best[0:num_poly_flippable])

    for(uint64_t polyy = 0; polyy < num_poly_flippable; polyy++) p->polymers[poly_flippable_indices[polyy]].type=poly_types_best[polyy];
    
#pragma acc exit data delete(poly_flippable_indices[0:num_poly_flippable])
#pragma acc exit data delete(poly_types_best[0:num_poly_flippable])

    free(poly_isflippable); 
    free(poly_cell_indices);
    free(poly_cell_num);
    free(poly_types_best);
    free(delta_fields_unified_best);
    free(poly_flippable_indices);
    return 0;
}

uint64_t get_flip_candidates(struct Phase * p, int64_t * poly_isflippable)
{
    uint64_t num_poly_flippable=0; //number of polymers that have monomers in conversion zone
    //loop over polymers to identify the ones that may be flipped
#pragma acc enter data copyin(poly_isflippable[0:p->n_polymers],num_poly_flippable)
#pragma acc parallel loop present(p[0:1],poly_isflippable[0:p->n_polymers]) reduction(+:num_poly_flippable)
    for (uint64_t poly = 0; poly < p->n_polymers; poly++)
        {
            const unsigned int N = p->reference_Nbeads; //polymer length (only if all polymers have the same length)
            //loop over monomers to get cell information while disregarding the monomer type for now
            int k=0;
            for (unsigned int mono = 0; mono < N; mono ++)
                {
                    const Monomer pos = ((Monomer *) p->ph.beads.ptr)[p->polymers[poly].bead_offset + mono];       //Read Monomer position
                    const uint64_t mono_cell = coord_to_index(p, pos.x, pos.y, pos.z);    //Read Monomer cell
                    //check if umbrella field is available for some (or more) types
                    for (unsigned int monotype = 0; monotype < p->n_types; monotype ++)
                        {
                            if(p->umbrella_field[monotype*p->n_cells_local + mono_cell] > 0)
                                {
                                    poly_isflippable[poly]=1;
                                    k=1;
                                    break;
                                }

                        }
                    if (k==1)
                        {
                            num_poly_flippable++;
                            break;
                        }
                }
        }
#pragma acc update self(poly_isflippable[0:p->n_polymers],num_poly_flippable)
#pragma acc exit data delete(poly_isflippable[0:p->n_polymers],num_poly_flippable)
    return num_poly_flippable;
}

void get_cell_info(struct Phase * p, uint64_t num_poly_flippable, int64_t * poly_flippable_indices, int64_t * poly_cell_indices, int64_t * poly_cell_num)
{
    //loop over flippable polymers and get all cell information for each possible type
    //use polyy instead of poly to distinguish from polymer index in actual polymer array
#pragma acc enter data copyin(num_poly_flippable,poly_flippable_indices[0:num_poly_flippable], poly_cell_indices[0:num_poly_flippable * p->reference_Nbeads], poly_cell_num[0:num_poly_flippable  * p->n_poly_type* p->n_types * p->reference_Nbeads])
#pragma acc parallel loop present(p[0:1],num_poly_flippable,poly_flippable_indices[0:num_poly_flippable], poly_cell_indices[0:num_poly_flippable * p->reference_Nbeads], poly_cell_num[0:num_poly_flippable  * p->n_poly_type* p->n_types * p->reference_Nbeads])
    for (uint64_t polyy = 0; polyy< num_poly_flippable; polyy++)
        {
            const unsigned int N = p->reference_Nbeads; //polymer length (only if all polymers have the same length)
            int64_t * mono_cells=(int64_t *)malloc( N *  sizeof(int64_t)); //Array of length p->reference_Nbeads that contains monomer cell indices of a polymer. Values are -1 if no target density available in that cell.
            uint64_t poly=poly_flippable_indices[polyy]; //actual polymer index
            unsigned int initial_poly_type = p->polymers[poly].type;
            //loop over monomers to get cell information (all cells)
            unsigned int j=0;
            for (unsigned int mono = 0; mono < N; mono ++)
                {
                    const Monomer pos = ((Monomer *) p->ph.beads.ptr)[p->polymers[poly].bead_offset + mono];       //Read Monomer position
                    const uint64_t mono_cell = coord_to_index(p, pos.x, pos.y, pos.z);    //Read Monomer cell
                    //check if target composition is available in that cell for any given type
                    for (unsigned int monotype = 0; monotype < p->n_types; monotype ++)
                        {
                            if(p->umbrella_field[monotype*p->n_cells_local + mono_cell] > 0)
                                {
                                    mono_cells[j]=mono_cell;
                                    j++;
                                    break;
                                }

                        }
                }
            //sort mono_cells array
            quicksort(mono_cells,0,j-1);
            //loop over monomers to get unique cells
            unsigned int k=0;
            for(unsigned int mono = 0; mono < j - 1 ; mono++)
                {
                    //update array only for new cells
                    if(mono_cells[mono]!= mono_cells[mono+1])
                        {
                            poly_cell_indices[polyy * N + k]=mono_cells[mono]; //unique monomer cell indices
                            k++;
                        }
                }

            poly_cell_indices[polyy * N + k]=mono_cells[j-1];


            //loop over polymer types to fill poly_cell_num array
            for(unsigned int polytype = 0; polytype < p->n_poly_type ; polytype++)
                {
                    //temporarily change polytype
                    p->polymers[poly].type = polytype;
                    //loop over monomers
                    for(unsigned int mono = 0; mono < N ; mono++)
                        {
                            const Monomer pos = ((Monomer *) p->ph.beads.ptr)[p->polymers[poly].bead_offset + mono];       //Read Monomer position
                            const uint64_t mono_cell = coord_to_index(p, pos.x, pos.y, pos.z);    //Read Monomer cell
                            unsigned int monotype = get_particle_type(p, poly, mono); //Read Monomer type
                            if(p->umbrella_field[monotype*p->n_cells_local + mono_cell] > 0)
                                {
                                    unsigned int mono_cell_offset=0; //offset of monomer cell in poly_cell_indices
                                    //Get cell offset for poly_cell_indices
                                    for(unsigned int cell_offset = 0; cell_offset < N; cell_offset++)
                                        {
                                            if(poly_cell_indices[polyy * N + cell_offset] == mono_cell)
                                                {
                                                    mono_cell_offset = cell_offset;
                                                    break;
                                                }
                                        }
                                    poly_cell_num[polyy * p->n_poly_type * p->n_types * N + polytype * p->n_types * N +monotype * N + mono_cell_offset]++; //increment according entry in poly_cells_num (tedious but I don't say another way)
                                }

                        }

                }

            //reset polymer type to original one
            p->polymers[poly].type = initial_poly_type;
            free(mono_cells);
        }
#pragma acc exit data copyout(num_poly_flippable,poly_flippable_indices[0:num_poly_flippable], poly_cell_indices[0:num_poly_flippable * p->reference_Nbeads], poly_cell_num[0:num_poly_flippable  * p->n_poly_type* p->n_types * p->reference_Nbeads])
    return;


}

void update_delta_fields(struct Phase * p, uint64_t polyy, unsigned int initial_type, unsigned int final_type, int64_t * poly_cell_indices, int64_t * poly_cell_num,int64_t * delta_fields_unified)
{ 
    const unsigned int N = p->reference_Nbeads;
    // loop over cell indices
    for(unsigned int mono_cell_offset = 0; mono_cell_offset < N; mono_cell_offset++)
        {
            if(poly_cell_indices[polyy  * N + mono_cell_offset] < 0) break; //check if end has been reached
            int64_t cell = poly_cell_indices[polyy * N + mono_cell_offset];
            uint64_t beads_in_cell = 0; //total number of beads in the cell
            //loop over monotypes to update delta_fields
            for(unsigned int monotype = 0; monotype < p->n_types; monotype++)
                {
                    if(p->umbrella_field[monotype*p->n_cells_local + cell] > 0)
                        {
                            //number of monomers of current type in current cell before and after flip
                            int64_t num_mono_initial = poly_cell_num[polyy * p->n_poly_type * p->n_types * N + initial_type * p->n_types * N + monotype * N + mono_cell_offset ];
                            int64_t num_mono_final = poly_cell_num[polyy * p->n_poly_type * p->n_types * N + final_type * p->n_types * N + monotype * N + mono_cell_offset ];
                            //difference of the two
                            int64_t delta_num_mono = num_mono_final - num_mono_initial;
                            delta_fields_unified[monotype*p->n_cells_local + cell] += delta_num_mono;
                        }
                }
        }
        
    return;
}


soma_scalar_t simulated_annealing(struct Phase * p,soma_scalar_t total_cost, uint64_t num_poly_flippable, uint64_t * total_flip_attempts, uint64_t * total_flips_accepted, int64_t * poly_cell_indices, int64_t * poly_cell_num, int64_t * poly_flippable_indices, int64_t * delta_fields_unified_best,unsigned int * poly_types_best)
{
    int64_t * delta_fields_unified = (int64_t *)malloc(p->n_types * p->n_cells_local * sizeof(int64_t));//array to store temporary delta_fields_unified 
    unsigned int * poly_types=(unsigned int *)malloc(num_poly_flippable * sizeof(unsigned int)); //array that stores temporary polymer types
    soma_scalar_t total_cost_old = total_cost;
    soma_scalar_t total_cost_best = total_cost;
    soma_scalar_t T = p->Tmax;
    soma_scalar_t acc_rate = 0;
    uint64_t num_poly_flipped=0; //number of accepted flips before improvement in cost function
    int64_t * poly_flipped_indices = (int64_t *)malloc( num_poly_flippable * sizeof(int64_t)); //array that contains indices of flipped polymers. The same polymer can be flipped multiple times. Indices are not the real polymer indices.

    // initialize delta_fields_unified and poly_types with current best values
    for (uint64_t cell = 0; cell < p->n_cells_local; cell++)
        {
            for(uint64_t type = 0; type < p->n_types; type++)
                {
                    delta_fields_unified[type*p->n_cells_local + cell] = delta_fields_unified_best[type*p->n_cells_local + cell];
                }
        }

        // inititalize poly_types
    for (uint64_t poly = 0; poly < num_poly_flippable; poly++)
        {
            poly_types[poly]=poly_types_best[poly];
        }
    const unsigned int N = p->reference_Nbeads; //maximum polymer length
    unsigned int temperature_counter=1;
    while(T > p->Tmin)
        {
            uint64_t flip_counter=0;
            uint64_t acceptance_counter=0;
            total_cost_old=total_cost;
            //do num_poly_flippable random flips at current temperature
            for(uint64_t i = 0; i< num_poly_flippable; i++)
                {
                    flip_counter++;
                    //choose random polymer to flip
                    uint64_t polyy = (uint64_t)(soma_rng_soma_scalar(&((p->polymers)->poly_state), p) * (soma_scalar_t)(num_poly_flippable - 1));
                    uint64_t poly = poly_flippable_indices[polyy];
                    Polymer *mypoly = p->polymers + poly;
                    unsigned int initial_type = poly_types[polyy];
                    unsigned int final_type = flip(p,poly,initial_type);
                    *total_flip_attempts=*total_flip_attempts+1;
                    
                    //calculate cost 

                    total_cost += get_flip_cost_composition(p, polyy, initial_type, final_type, poly_cell_indices, poly_cell_num, delta_fields_unified);

                    soma_scalar_t random_number = soma_rng_soma_scalar(&(mypoly->poly_state), p);
                    //check acceptance criterion
                    if((total_cost < total_cost_old) || (random_number < exp(-(total_cost - total_cost_old)/T)))
                        {
                            //accept flip
                            acceptance_counter++;
                            //write index of flipped polymer to poly_flipped_indices if not too many have been flipped already
                            if(num_poly_flipped < num_poly_flippable)
                                {
                                    poly_flipped_indices[num_poly_flipped]=polyy;
                                }
                            num_poly_flipped++;
                            *total_flips_accepted=*total_flips_accepted+1;
                            poly_types[polyy]=final_type;
                            total_cost_old=total_cost;
                            //update delta fields unified
                            update_delta_fields(p, polyy, initial_type, final_type, poly_cell_indices, poly_cell_num, delta_fields_unified);

                        }
                    else
                        {
                            //reject
                            total_cost = total_cost_old;
                        }



                    //update best solution so far
                    if (total_cost < total_cost_best)
                        {
                            total_cost_best=total_cost;
                            //loop over flipped polymers
                            //if more than num_poly_flippable polymers have been flipped, simply update all polytypes
                            if (num_poly_flipped > num_poly_flippable)
                                {
                                    for(uint64_t j = 0; j < num_poly_flippable; j++)
                                        {
                                            poly_types_best[j]=poly_types[j];
                                            for(unsigned int i = 0; i < N; i++)
                                                {
                                                    if(poly_cell_indices[j * N + i] < 0) break;
                                                    unsigned int cell = poly_cell_indices[j * N + i];
                                                    for(unsigned int type = 0; type < p->n_types; type++) delta_fields_unified_best[type*p->n_cells_local + cell] = delta_fields_unified[type*p->n_cells_local + cell];
                                                }
                                        }
                                }
                            // else, update only the ones saved in poly_flipped_indices. On average, this is much quicker than updating all polytypes
                            else
                                {
                                    for(uint64_t j = 0; j < num_poly_flipped; j++)
                                        {
                                            poly_types_best[poly_flipped_indices[j]]=poly_types[poly_flipped_indices[j]];
                                            poly=poly_flipped_indices[j];
                                            for(unsigned int i = 0; i < N; i++)
                                                {
                                                    if(poly_cell_indices[poly * N + i] < 0) break;
                                                    unsigned int cell = poly_cell_indices[poly * N + i];
                                                    for(unsigned int type = 0; type < p->n_types; type++) delta_fields_unified_best[type*p->n_cells_local + cell] = delta_fields_unified[type*p->n_cells_local + cell];
                                                }
                                        }
                                }

                            //reset flip counter   
                            num_poly_flipped=0;
                        }

                }
            acc_rate=(soma_scalar_t)acceptance_counter/flip_counter;
            printf("T: %lf\n",T);
            printf("Accrate: %lf\n",acc_rate);
            printf("Best value: %lf\n" , total_cost_best);
            //update temperature
            //T *= p->alpha; //exponential cooling
            //T = p->Tmax/(1.0+p->alpha*(soma_scalar_t)log(1.0+(soma_scalar_t)temperature_counter)); //linear multiplicative cooling
            temperature_counter++;
            T-=p->alpha; //linear cooling

        }
    //take best solution
    free(poly_flipped_indices);
    free(delta_fields_unified);
    free(poly_types);
    return total_cost_best;
}




soma_scalar_t flip_polytypes(struct Phase * p,soma_scalar_t total_cost, uint64_t num_poly_flippable, uint64_t * total_flip_attempts, uint64_t * total_flips_accepted, int64_t * poly_cell_indices, int64_t * poly_cell_num, int64_t * poly_flippable_indices, int64_t * delta_fields_unified_best ,unsigned int * poly_types_best)
{
    const unsigned int N = p->reference_Nbeads; //monomers per polymer
    soma_scalar_t total_cost_old = total_cost;
    soma_scalar_t acc_rate = 1.0;
    uint64_t flip_counter =0; //counts number of flips 
    uint64_t flip_counter_acc =0; //counts number of accepted flips  
    soma_scalar_t tol=1e-15; //tolerance for accepting flips
    uint64_t acc_rate_freq = (uint64_t)num_poly_flippable; // calculate acceptance rate per num_poly_flippable flips
    while((acc_rate > 0.0))
        {
            total_cost=total_cost_old;
            //printf("%lf\n",total_cost);
            //choose random polymer to flip
            flip_counter++;
            uint64_t polyy = rand() % (num_poly_flippable - 1);
            uint64_t poly = poly_flippable_indices[polyy]; //real polymer index
            Polymer *mypoly = p->polymers + poly;
            unsigned int initial_type = poly_types_best[polyy];
            unsigned int final_type = flip(p,poly,initial_type);
            *total_flip_attempts=*total_flip_attempts+1;
            //calculate cost 
            total_cost += get_flip_cost_composition(p, polyy, initial_type, final_type, poly_cell_indices, poly_cell_num, delta_fields_unified_best);
            //check acceptance criterion
            //sometimes it gets stuck at very small changes in loss function, probably numerical error -> only accept if difference is above threshold
            if((total_cost < total_cost_old) && (total_cost_old - total_cost)>tol)
                {
                    flip_counter_acc++;
                    *total_flips_accepted=*total_flips_accepted+1;
                    //accept flip
                    poly_types_best[polyy]=final_type;
                    total_cost_old=total_cost;
                    //update delta fields unified
                    update_delta_fields(p, polyy, initial_type, final_type, poly_cell_indices, poly_cell_num, delta_fields_unified_best);

                }
            else
                {
                    //reject
                    total_cost = total_cost_old;
                }
            if(flip_counter % acc_rate_freq == 0)
                {
                    acc_rate = (soma_scalar_t)(flip_counter_acc)/(soma_scalar_t)(flip_counter);
                    flip_counter=0;
                    flip_counter_acc=0;
                }
             
            //printf("MSE: %.15lf \n",total_cost);
        }
    //give each polymer one more chance to flip
    for(uint64_t polyy=0; polyy < num_poly_flippable; polyy++ )
        {
            total_cost=total_cost_old;
            uint64_t poly = poly_flippable_indices[polyy]; //real polymer index
            Polymer *mypoly = p->polymers + poly;
            unsigned int initial_type = poly_types_best[polyy];
            unsigned int final_type = flip(p,poly,initial_type);
            *total_flip_attempts=*total_flip_attempts+1;
            //calculate cost 
            total_cost += get_flip_cost_composition(p, polyy, initial_type, final_type, poly_cell_indices, poly_cell_num, delta_fields_unified_best);
            //check acceptance criterion
            //need to be careful for total_cost == total_cost_old, the loop gets stuck -> that's why tol value is needed
            if((total_cost < total_cost_old) && (total_cost_old - total_cost)>tol)
                {
                    flip_counter_acc++;
                    *total_flips_accepted=*total_flips_accepted+1;
                    //accept flip
                    poly_types_best[polyy]=final_type;
                    total_cost_old=total_cost;
                    //update delta fields unified
                    update_delta_fields(p, polyy, initial_type, final_type, poly_cell_indices, poly_cell_num, delta_fields_unified_best);

                }
            else
                {
                    //reject
                    total_cost = total_cost_old;
                }                    
        }

    return total_cost;
}


soma_scalar_t get_cost_composition(struct Phase *p)
{  
    soma_scalar_t total_cost=0.0;
    //loop over monomer types
//#pragma acc parallel loop present(p[0:1])
    for(unsigned int type = 0; type < p->n_types; type++)
        {
            //loop over cells
            for (uint64_t cell = 0; cell < p->n_cells_local; cell++)
                {
                    //update cost
                    if(p->umbrella_field[type*p->n_cells_local + cell] > 0)
                        {
                            //get number of beads in cell
                            uint16_t beads_in_cell = 0; 
                            for(unsigned int i = 0; i < p->n_types; i++) beads_in_cell += p->fields_unified[i*p->n_cells_local + cell];
                            //check if there are no beads in the cell, in that case loss is simply square of umbrella field
                            if (beads_in_cell==0) total_cost+=p->umbrella_field[type*p->n_cells_local + cell] * p->umbrella_field[type*p->n_cells_local + cell];
                            else total_cost+=( (soma_scalar_t)p->umbrella_field[type*p->n_cells_local + cell]-(soma_scalar_t)( p->fields_unified[type*p->n_cells_local + cell]) /beads_in_cell ) * ( (soma_scalar_t)p->umbrella_field[type*p->n_cells_local + cell]-(soma_scalar_t)( p->fields_unified[type*p->n_cells_local + cell]) /beads_in_cell ) ;
                        }
                        
                }
        }
    return total_cost;
}


soma_scalar_t get_flip_cost_composition(struct Phase * p, uint64_t polyy, unsigned int initial_type, unsigned int final_type, int64_t * poly_cell_indices, int64_t * poly_cell_num,int64_t * delta_fields_unified)
{
    
    soma_scalar_t delta_cost = 0.0; //change in cost function
    const unsigned int N = p->reference_Nbeads; //maximum polymer length

    // loop over target cell indices
    for(unsigned int mono_cell_offset = 0; mono_cell_offset < N; mono_cell_offset++)
        {
            if(poly_cell_indices[polyy  * N + mono_cell_offset] < 0) break; //check if end has been reached
            int64_t cell = poly_cell_indices[polyy * N + mono_cell_offset];
            uint64_t beads_in_cell = 0; //total number of beads in the cell
            //get beads in cell
            for(unsigned int monotype = 0; monotype < p->n_types; monotype++) beads_in_cell += p->fields_unified[monotype*p->n_cells_local + cell];
            //loop over monotypes to get delta_cost
            for(unsigned int monotype = 0; monotype < p->n_types; monotype++)
                {
                    //number of monomers of current type in current cell before and after flip
                    int64_t num_mono_initial = poly_cell_num[polyy * p->n_poly_type * p->n_types * N + initial_type * p->n_types * N + monotype * N + mono_cell_offset];
                    int64_t num_mono_final = poly_cell_num[polyy * p->n_poly_type * p->n_types * N + final_type * p->n_types * N + monotype * N + mono_cell_offset];
                    //composition difference
                    if(num_mono_final!=num_mono_initial)
                        {
                            soma_scalar_t delta_comp = (soma_scalar_t)(num_mono_final - num_mono_initial)/(soma_scalar_t)beads_in_cell;
                            // //update cost function
                            delta_cost+=delta_comp * delta_comp + 2.0*delta_comp*((soma_scalar_t)(p->fields_unified[monotype*p->n_cells_local + cell] + 
                                        (soma_scalar_t)delta_fields_unified[monotype*p->n_cells_local + cell])/(soma_scalar_t)beads_in_cell - (soma_scalar_t)p->umbrella_field[monotype*p->n_cells_local + cell] );
                        }

                   
                }
        }

    //if(delta_cost<0) printf("%.20lf\n",delta_cost);
    return delta_cost;
}


unsigned int flip(struct Phase * p, uint64_t poly, unsigned int initial_type)
{
    unsigned int * target_types=(unsigned int *)malloc( (p->n_poly_type - 1) *  sizeof(unsigned int));
    unsigned int k=0;
    Polymer *mypoly = p->polymers + poly; //needed only for rng
    //get array with every type except current one
    for(unsigned int polytype=0; polytype < p->n_poly_type;polytype++)
        {
            if(polytype != initial_type)
                {
                    target_types[k]=polytype;
                    k++;
                }
        }
    
    //draw random index in range length(target_types)
    unsigned int random_idx = (unsigned int)(soma_rng_soma_scalar(&(mypoly->poly_state), p)* (soma_scalar_t)k);
    
    unsigned int final_type=target_types[random_idx];
    free(target_types);
    return final_type;

}

void quicksort(int64_t * array ,int first, int last){
   int i, j;
   int64_t pivot, temp;

   if(first<last){
      pivot=first;
      i=first;
      j=last;

      while(i<j){
         while(array[i]<=array[pivot]&&i<last)
            i++;
         while(array[j]>array[pivot])
            j--;
         if(i<j){
            temp=array[i];
            array[i]=array[j];
            array[j]=temp;
         }
      }

      temp=array[pivot];
      array[pivot]=array[j];
      array[j]=temp;
      quicksort(array,first,j-1);
      quicksort(array,j+1,last);

   }
}



